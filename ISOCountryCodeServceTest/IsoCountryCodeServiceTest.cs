﻿namespace ISOCountryCodeServceTest
{
    using System;
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RestSharp;

    [TestClass]
    public class IsoCountryCodeServiceTest
    {
        private static IsoCountryCodeClient _isoCountryCodeClient;

        /// <summary>
        /// Calls a REST service to get a list of valid 2 character country ISO codes. Then using a random code from that request
        /// calls the service under test, a REST service which gets a single country's data from the 2 character ISO code.
        /// Test verifies that the response is the single expected Country with correct capitalization and the correct amount of characters
        /// </summary>
        [TestMethod]
        public void RequestMultipleCountriesAndVerifyFormatting()
        {
            _isoCountryCodeClient = new IsoCountryCodeClient(new RestClient("http://services.groupkt.com/country/"));

            var randomCountry = GetRandomCountry();

            var countryResponse = _isoCountryCodeClient.GetCountryByAlpha2Code(randomCountry.Alpha2_Code);

            MessageAssertion(randomCountry.Alpha2_Code, countryResponse.Messages);
            CountryAssertion(randomCountry, countryResponse.Result);
        }

        public Country GetRandomCountry()
        {
            var allCountriesResponse = _isoCountryCodeClient.GetAllCountries().Result;

            var randomCountry = allCountriesResponse[new Random().Next(allCountriesResponse.Count)];

            return randomCountry;
        }

        public void MessageAssertion(string randomAlpha2Code, List<string> responseMessages)
        {
            foreach (var message in responseMessages)
            {
                Assert.IsFalse(message.StartsWith("No matching country found for requested code"), $"Invalid Country Alpha2 Code entered: {randomAlpha2Code}");
            }
        }

        public void CountryAssertion(Country expectedCountry, Country gotCountry)
        {
            Assert.IsNotNull(gotCountry.Name, "Name is null, returned property did not map properly");
            Assert.AreEqual(expectedCountry.Name, gotCountry.Name, "Expected and Actual Country Names do not match");
            Assert.IsTrue(Char.IsUpper(gotCountry.Name[0]), "Country name is not capitalized");

            Assert.IsNotNull(gotCountry.Alpha2_Code, "Alpha2_Code is null, returned property did not map properly");
            Assert.AreEqual(expectedCountry.Alpha2_Code, gotCountry.Alpha2_Code, "Expected and Actual Country Alpha2_Codes do not match");
            Assert.IsTrue(gotCountry.Alpha2_Code.Length == 2, "Country Alpha2 Code is not 2 characters long");
            foreach (var character in gotCountry.Alpha2_Code.ToCharArray())
            {
                Assert.IsTrue(Char.IsUpper(character), "Country Alpha2 Code is not all capital letters");
            }

            Assert.IsNotNull(gotCountry.Alpha3_Code, "Alpha3_Code is null, returned property did not map properly");
            Assert.AreEqual(expectedCountry.Alpha3_Code, gotCountry.Alpha3_Code, "Expected and Actual Country Alpha3_Codes do not match");
            Assert.IsTrue(gotCountry.Alpha3_Code.Length == 3, "Country Alpha3 Code is not 3 characters long");
            foreach (var character in gotCountry.Alpha3_Code.ToCharArray())
            {
                Assert.IsTrue(Char.IsUpper(character), "Country Alpha3 Code is not all capital letters");
            }
        }
    }
}