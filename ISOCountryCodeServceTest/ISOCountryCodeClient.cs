﻿namespace ISOCountryCodeServceTest
{
    using Models;
    using Newtonsoft.Json;
    using RestSharp;

    class IsoCountryCodeClient
    {
        public IsoCountryCodeClient(IRestClient restClient)
        {
            RestClient = restClient;
        }

        private IRestClient RestClient { get; }

        public SingleResultRestResponse GetCountryByAlpha2Code(string alpha2Code)
        {
            var countryRequest = new RestRequest($"get/iso2code/{alpha2Code}", Method.GET);

            countryRequest.AddHeader("Content-Type", "application/json");

            var countryResponse = RestClient.Execute(countryRequest);

            return JsonConvert.DeserializeObject<SingleResultResponseRoot>(countryResponse.Content).RestResponse;
        }

        public MultipleResultRestResponse GetAllCountries()
        {
            var countryRequest = new RestRequest("get/all", Method.GET);

            countryRequest.AddHeader("Content-Type", "application/json");

            var countryResponse = RestClient.Execute(countryRequest);

            return JsonConvert.DeserializeObject<MultipleResultResponseRoot>(countryResponse.Content).RestResponse;
        }
    }
}
