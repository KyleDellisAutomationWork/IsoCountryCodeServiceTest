﻿namespace ISOCountryCodeServceTest.Models
{
    using System.Collections.Generic;

    public class SingleResultRestResponse
    {
        public List<string> Messages { get; set; }
        public Country Result { get; set; }
    }
}