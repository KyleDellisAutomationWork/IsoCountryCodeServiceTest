﻿namespace ISOCountryCodeServceTest
{
    using System.Collections.Generic;

    public class MultipleResultRestResponse
    {
        public List<string> Messages { get; set; }
        public List<Country> Result { get; set; }
    }
}
